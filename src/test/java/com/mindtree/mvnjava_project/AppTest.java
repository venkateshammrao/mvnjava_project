package com.mindtree.mvnjava_project;

import org.junit.Assert;
import org.junit.Test;
import java.io.*;

/**
 * Unit test for simple App.
 */
public class AppTest {

	@Test
	public void testAddition(){
		int additionValue;
		App appObj = new App();
		Assert.assertTrue("Addition is not succesfull", appObj.getAddition(4, 5) == (4+5));
	}

	@Test
	public void testSubstraction(){
		App appObj = new App();
		Assert.assertTrue("Substraction is not succesfull", appObj.getSubstraction(7, 3) == (7-3));
	}
}
